<?php

include "users.php";
include "course.php";
include "assets.php";
include "matters.php";
include "teachers.php";
include "teacher_course.php";

/***************************************************************************************/

/**
* ####################-ALL NEED AUTHENTICATION-###########################################
*/
Route::group(["middleware" => ["auth","web","role:admin|teacher|superadmin"],"prefix"=>"bo"], function(){

    Route::get('get_list_user_role/{id}',"BackOffice\RolesController@UserHasRoles")
        ->name('get_list_user_role');

});
/**
*
* All user access that has the superadministrator role
*
*/
Route::group(["middleware"=>['role:superadmin',"web","auth"],"prefix" => 'super-admin'], function(){


});


Route::group(["middleware" => ['role:admin','web','auth'], 'prefix' => 'admin'], function() {

    /*************************** The User managment ********************************/

    Route::get('/', "BackOffice\Admin\UsersController@index")
        ->name("admin.users.index");

    Route::get('/users', "BackOffice\Admin\UsersController@index")
        ->name("admin.users.index");

    Route::post("/user/create", "BackOffice\Admin\UsersController@create")->name('admin.user.create');

    /**********************************************************************/
    Route::get("manage/roles", "BackOffice\RolesController@manage")
        ->name("admin.roles.list");


    });
/**
* All user access that has the administrator or superadministrator role
*
*/
Route::group(["middleware"=>["role:admin|superadmin"],"prefix" => 'bo/admin',"web","auth"], function(){

/*********************** Studiant USER *****************************************/



    Route::post("/studiant/add", "BackOffice\StudiantController@store")
    ->name("admin.studiant.add");

    Route::get("user", "BackOffice\StudiantController@index")
        ->name('manager.studiant.user');

    Route::post("/studaint/delete/{id}", "BackOffice\StudiantController@delete")
    ->name("admin.studiant.delete");

    Route::get("/studaint/edit/{id}", "BackOffice\StudiantController@edit")
    ->name("admin.studiant.edit");

    Route::post("/studaint/save_edit/{id}", "BackOffice\StudiantController@save_edit")
    ->name("admin.studiant.save_edit");


/************************* End Studiant ****************************************/


/*************************** End of The User Creation *****************************/

    Route::post("/role/assignment", "BackOffice\RolesController@assignment")
        ->name("admin.role.assignment");

    Route::get("manage/roles", "BackOffice\RolesController@manage")
        ->name("admin.roles.list");

    Route::get("manage/permission", "BackOffice\PermissionController@index")
        ->name("admin.permission.list");

    Route::post("manage/permission/add", "BackOffice\PermissionController@create")
        ->name("admin.permission.add");

});


/**
* Anyone authenticated
*
*/
Route::group(["middleware"=>["web","auth"],"prefix" => 'authenticated'], function(){

    Route::get('dashboard', "BackOffice\DashboardController@manager")
        ->name("backOffice.dashboard");

});

/**
* ####################-END ALL NEED AUTHENTICATION-###########################################
*/
Auth::routes();

/**
* ####################-ALL DO NOT NEED AUTHENTICATION-####################################
*/

Route::get('/', 'HomeController@index')
    ->name('public.home');

Route::get('/news', 'CourseController@news')
    ->name('public.news');

Route::get('/contact', 'ContactController@index')
    ->name('public.contact');

/*******************************************************************/

Route::get('/error404', 'ErrorController@index404')
    ->name('error404');

Route::get('/logout', 'Auth\LoginController@logout')
    ->name('logout');


/**
* ####################-END ALL DO NOT NEED AUTHENTICATION-####################################
*/
Auth::routes();

Route::get('/home', 'HomeController@index')
    ->name('home');
