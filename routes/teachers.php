<?php

/**
* All user access that has the administrator or superadministrator role
*
*/
Route::group(["middleware"=>["role:admin|superadmin"],"prefix" => 'bo/admin',"web","auth"]
, function(){


    Route::get("/teacher/list", "BackOffice\TeacherController@index")
    ->name("admin.teacher.list");

    Route::post("/teacher/add", "BackOffice\TeacherController@store")
    ->name("admin.teacher.add");

    Route::get("/teacher/edit/{id}", "BackOffice\TeacherController@edit")
    ->name("admin.teacher.edit");


});

