<?php


Route::group(["middleware" => ['role:admin','web','auth'], 'prefix' => 'admin']
, function() {

    /** -------------------------MATTER----------------------------------------- **/

    Route::get('/matter/list', 'BackOffice\Admin\MatterAdminController@index')
        ->name('bo.prof.matter.list');

    Route::get('/matter/show/{id}', 'BackOffice\Admin\MatterAdminController@show')
        ->name('bo.prof.matter.show');

    Route::get('/matter/create', 'BackOffice\Admin\MatterAdminController@create')
        ->name('bo.prof.matter.new');

    Route::post('/matter/store', 'BackOffice\Admin\MatterAdminController@store')
        ->name('bo.prof.matter.store');

    Route::get('/matter/edit/{id}', 'BackOffice\Admin\MatterAdminController@edit')
        ->name('bo.prof.matter.edit');

    Route::post('/matter/update/{id}', 'BackOffice\Admin\MatterAdminController@update')
        ->name('bo.prof.matter.update');

    Route::get('/matter/destroy/{id}', 'BackOffice\Admin\MatterAdminController@destroy')
        ->name('bo.prof.matter.destroy');

    Route::get('/matter/actions/{id}', 'BackOffice\Admin\MatterAdminController@actions')
        ->name('bo.prof.matter.actions');

    Route::post('/matter/assigner/{id}', 'BackOffice\Admin\MatterAdminController@assigner')
        ->name('bo.prof.matter.assigner');

    Route::post('/matter/delete/{id}', "BackOffice\Admin\MatterAdminController@delete")
        ->name('bo.prof.matter.delete.confirmed');

    Route::get("/matter/course/destroy/{id}/matter/{matter_id}",
     "BackOffice\Admin\MatterAdminController@course_destroy")
        ->name('bo.prof.matter.course_destroy');

});
?>
