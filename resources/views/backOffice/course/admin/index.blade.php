
@extends('layouts.backOffice.adminLTE')

@section('content')

<section class="content-header">
    <h1>
        {{ config('app.name') }}
        <small>Recherche</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        {{--
            <li><a href="#">Forms</a></li>
        --}}
        <li class="active">home</li>
    </ol>
</section>
@include('backOffice.course.admin.rupture')
@include('backOffice.course.admin.assignment_course')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                  <h3 class="box-title">Cours</h3>
                </div>
                    
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        
                        <div class="row">
                            <div class="col-sm-12">
                               
                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                            id</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">nom</th>
                                            <th>Professeur lié</th>
                                            <th colspan="1"> Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($courses as $key => $course)
                                            <tr role="row" class="odd">
                                                <td class="sorting_1">{{ $course->id }}</td>
                                                <td>{{ $course->title }}</td>
                                                <td>
                                                    @if (!empty($course->name))
                                                    
                                                        {{ $course->name }}

                                                    @endif
                                                </td>
                                                <td>
                                                    {{-- {{ route('bo.admin.course.actions', $course->id) }}  --}}

                                                    @if (empty($course->name))
                                                    
                                                        <a href="{{ route('bo.admin.course.save_assignment') }}" class="btn btn-info  new-assignment"
                                                        data-id="{{ $course->id }}"
                                                        data-url="">
                                                            Assigner
                                                        </a>

                                                    @else
                                                    {{-- red --}}
                                                        <a href="{{ route('bo.admin.course.rupture_assignation') }}" class="btn btn-danger new-rupture"
                                                        data-id="{{ $course->id }}"
                                                        >
                                                            Rupture
                                                        </a>
                                                    @endif

                                                    
                                                </td>
                                                
                                            </tr>
                                        @endforeach

                                        @if ($course->count() > 5)
                                            <tr>
                                                <td colspan="3" style="text-align: center;">
                                                    {{ $courses->links() }}
                                                </td>
                                            </tr>
                                        @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </div>
</section>

@stop

@section('javascript')

<!-- <script src="{{ asset('js/page.js') }}"></script> -->
<script src="{{ asset('js/script.js') }}" type="text/javascript"></script>

<script type="text/javascript">

// if (document.body.nodeType === document.ELEMENT_NODE) {
//     console.log("Body est un noeud élément");
// } else {
//     console.log("Body est un noeud textuel");
// }


    function afficher_teacher(teacher)
    {
        if (typeof(teacher) == 'object' && teacher.hasOwnProperty('url'))
        {
            console.log(teacher.url);

            $.ajax({
                method: "POST",
                url     : teacher.url,
                data    : {
                    "_token" : '{{ csrf_token() }}',
                },
                dataType : "json",
                success  : (response) => {
                    
                    // $('#name').html(response.name);
                    // $('#email').html(response.email);
 
                }
            });
        }
    }


// Accès au premier enfant du noeud body
    var list_nodes = document.querySelectorAll('#teachers_for_assignment label');

// console.log(Array.isArray(list_nodes))
    
    // console.log(list_nodes)
    var inputs = [];

    if (Array.isArray(Array.from(list_nodes)) )
    {
        Array.from(list_nodes).forEach(element => {
           
            if (element.hasChildNodes())
            {
                
                if ('INPUT' == element.childNodes[1].nodeName)
                    {
                        var value = element.childNodes[1].getAttribute('value');
                        var url = element.childNodes[1].getAttribute('data-url');
                        
                        inputs.push({value : value, url: url})

                    }
            }
        });
    }








// Affiche les noeuds enfant du noeud body
// for (var i = 0; i < list_nodes.length; i++) {
//     if (typeof(list_nodes[i]) == 'object')
//     {
//         console.log(list_nodes[i].childNodes[1])
//     }
//     // console.log(list_nodes[i]);
// }

$(document).on('click', ".new-rupture",function(event){
    event.preventDefault();

    var action = $(this).attr('href');
    $('#form-rupture').attr('action', action);
    $('#rupture_user_id').val($(this).data('id'));
    // alert('new-rupture')
    $('#modal-rupture').modal('show');

});

$(document).on('click', ".new-assignment", function(event){
    event.preventDefault();
    
    var action = $(this).attr('href');

    if ($(this).data('url') != null)
    {
        // afficher_teacher({'url': $(this).data('url')});
    }


    $('#form-assignment').attr('action', action);
  
    $('#course_id').val($(this).data('id'));

    $('#modal-assignment').modal('show');
});

$(document).ready(function() {
    $("#sex-homme").prop("checked", true);
});


$('#search-page').script();

/**
--------------------------------------------------------------------------------


/**
---------------------------------------------------------------------------------
 */
//Date range picker
$('#reservation').daterangepicker()
    //Date range picker with time picker
$('#reservationtime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        format: 'MM/DD/YYYY h:mm A'
    })


//Date picker
$('#datepicker').datepicker({
    autoclose: true
})

</script>
@stop