<!-- /.box-header -->
<div class="modal fade" id="modal-assignment">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form-assignment" method="post" action="">
            <!-- form -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-shadow">Assignation d'un cours pour un professeur</h4>
                </div>
                <div class="modal-body">
                    <!-- form start -->

                        {{ csrf_field() }}
                        
                        <div class="box-body">
                            <div class="premary-content">
                                <!--  -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="select">
                                            <div class="form-group">
                                                <div>
                                                    
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--  -->
                                <!--  -->
                                <div class="row content-list">
                                    <div class="col-md-6">
                                        <h2>{{ ucfirst($person_form['personal_infos']) }}</h2>

                                        <div id="name"></div> 
                                        <div id="email"></div> 
                                    </div>
                                    <div class="col-md-6">
                                        <h2>Liste Proffesseurs</h2>
                                        <br>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input id="course_id" type="hidden" name="assignment_course[course_id]" >
                                        <div id="teachers_for_assignment">
                                            @foreach ($teachers as $key => $teacher)
                                                <label class="radio" for="teacher_{{ $teacher->uid }}"> 
                                                    <input class="teacher" 
                                                    id="teacher_{{ $teacher->uid }}" 
                                                    type="radio" name="assignment_course[user_id]" value="{{ $teacher->uid }}" 
                                                    >
                                                    <span> {{ $teacher->name }} </span>

                                                </label>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <!--  -->
                            </div>
                        </div>
                    <!-- /.box-body -->


                <!-- end modal-body -->
                </div>
                <div class="modal-footer">
                    
                    <button type="submit" class="btn btn-primary" id="save-studiant">Assigner</button>
                    <!-- data-dismiss="modal" cd code sert à dissimulé le modal -->
                </div>
            <!-- /form -->
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<style>
.radio {
  margin: 16px 0;
  display: block;
  cursor: pointer;
}
.radio input {
  display: none;
}
.radio input + span {
  line-height: 22px;
  height: 22px;
  padding-left: 22px;
  display: block;
  position: relative;
}
.radio input + span:not(:empty) {
  padding-left: 30px;
}
.radio input + span:before, .radio input + span:after {
  content: '';
  width: 22px;
  height: 22px;
  display: block;
  border-radius: 50%;
  left: 0;
  top: 0;
  position: absolute;
}
.radio input + span:before {
  background: #D1D7E3;
  transition: background 0.2s ease, transform 0.4s cubic-bezier(0.175, 0.885, 0.32, 2);
}
.radio input + span:after {
  background: #fff;
  transform: scale(0.78);
  transition: transform 0.6s cubic-bezier(0.175, 0.885, 0.32, 1.4);
}
.radio input:checked + span:before {
  transform: scale(1.04);
  background: #5D9BFB;
}
.radio input:checked + span:after {
  transform: scale(0.4);
  transition: transform 0.3s ease;
}
.radio:hover input + span:before {
  transform: scale(0.92);
}
.radio:hover input + span:after {
  transform: scale(0.74);
}
.radio:hover input:checked + span:after {
  transform: scale(0.4);
}

.radio-inline .radio {
    /* float:left; */
}
.textarea {
    max-width: 100%;
    height: 100%;
    min-height:205px;
}

</style>