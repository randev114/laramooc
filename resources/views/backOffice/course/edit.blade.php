@extends('layouts.backOffice.adminLTE')


@section('content')

<section class="content-header">
    <h1>
        {{ config('app.name') }}
        <small>Recherche</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        {{--
            <li><a href="#">Forms</a></li>
        --}}
        <li class="active">home</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">

                <div class="box-header">
                  <h3 class="box-title">Nouveau cours</h3>
                </div>
                    
                <!-- /.box-header -->
                <div class="box-body">
                    <form class="form-horizontal" action="{{ route('bo.prof.course.update', $course->id) }}" method="post">
                        
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-md-12 form-group">

                            <select name="course[matter_id]" class="form-control">
                                <option>---</option>
                                
                                @foreach($matters as $key => $matter)

                                
                                	@if(!is_null($course->matter()->first()) && $course->matter()->first()->id == $matter->id)
                                		<option selected="true" value="{{ $matter->id }}">
                                            {{ $matter->name }}
                                        </option>
                                    	@else
                                    	<option value="{{ $matter->id }}">{{ $matter->name }}</option>
                                   	@endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>Title</label>
                            <input name="course[title]" value="{{ $course->title }}" class="form-control">
                        </div>

                        <div class="col-md-12 form-group" >
                            <label>Contenu</label>
                            <textarea style="max-width: 100%;min-width: 100%;min-height: 200px;" class="form-control" name="course[content]">{{ $course->content }}</textarea>
                        </div>

                        <button value="submit" class="btn btn-info">Enregister</button>
                        

                    </form>
                    
                <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </div>
</section>
@stop
