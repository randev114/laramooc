
@extends('layouts.backOffice.adminLTE')



@section('content')

<section class="content-header">
    <h1>
        {{ config('app.name') }}
        <small>Recherche</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        {{--
            <li><a href="#">Forms</a></li>
        --}}
        <li class="active">home</li>
    </ol>
</section>

@include('backOffice.permission.new')

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                  <h3 class="box-title">Permissions</h3>
                  <button class="btn btn-success" id="new-permission"> <i class="fa fa-plus"></i></button>
                </div>
                    
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div><div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                            id</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">nom</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($permissions as $key => $permission)
                                            <tr role="row" class="odd">
                                              <td class="sorting_1">{{ $permission->id }}</td>
                                              <td>{{ $permission->name }}</td>
                                             
                                            </tr>
                                        @endforeach

                                        {{ $permissions->links() }}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </div>
</section>
@endsection

@section('javascript')
<script type="text/javascript">
    
    $(document).on('click', "#new-permission", function(event){
        event.preventDefault();
        $('#modal-teacher').modal('show');
    });
</script>

@endsection