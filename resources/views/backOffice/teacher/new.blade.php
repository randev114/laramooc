 <!-- /.box-header -->
<div class="modal fade" id="modal-teacher">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="{{ route('admin.teacher.add') }}">
            <!-- form -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-shadow">New teacher</h4>
                </div>
                <div class="modal-body">
                    <!-- form start -->

                        {{ csrf_field() }}
                        
                        <div class="box-body">
                            <div class="premary-content">
                                <!--  -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="select">
                                            <div class="form-group">
                                                <div>
                                                    
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--  -->
                                <!--  -->
                                <div class="row content-list">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="content">{!! ucfirst($person_form['name']) !!}</label>
                                            <input type="" name="person[name]" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="content">{!! ucfirst($person_form['prename']) !!}</label>
                                            <input type="" name="person[firstname]" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['email']) }}</label>
                                            <input value="" type="text" name="person[email]" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['nationality']) }}</label>
                                            <input value=""  type="text" name="person[nationality]" class="form-control" autocomplete="off">
                                            
                                        </div>

                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['birthday']) }}</label>
                                            <input value="" id="birthday" type="" name="person[birthday]" class="form-control" autocomplete="off">
                                            <div style="position: relative; ">
                                                <div id="birthDayContainer" style="position: absolute;"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['sex']['label']) }}</label>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="radio">
                                                        <input id="sex-homme" type="radio" name="person[sex]" selected=true value=1>
                                                        <span>{{ ucfirst($person_form['sex']['mal']) }}</span>
                                                    </label>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="radio">
                                                        <input id="sex-femme" type="radio" name="person[sex]" class="" value="2">
                                                        <span>{{ ucfirst($person_form['sex']['femal']) }}</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['country']) }}</label>
                                            <input type="text" name="person[country]" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="content">{{ucfirst($person_form['city']) }}</label>
                                            <input type="" name="person[city]" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['phone_number']) }}</label>
                                            <input type="" name="person[phone_number]" class="form-control">
                                        </div> 
                                    </div>
                                    <div class="col-md-6">
                                        
                                        <div class="form-group">
                                            <label for="content">{!! ucfirst($person_form['teacher']['matter']) !!}</label>
                                            <select class="form-control" name="teacher[matter_id]">

                                                @foreach (config('datamiror.matters') as $key => $matter)
                                                    <option value="{{ $key }}">{{ $matter }}</option>
                                                @endforeach
                                                
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="content">{!! ucfirst($person_form['teacher']['class']) !!}</label>
                                            <select class="form-control" name="teacher[class_id]">
                                                {{-- <option value="1"> 1 ère anné</option> --}}
                                                @foreach (config('datamiror.class') as $key => $class)
                                                    <option value="{{ $key }}">{{ $class }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <!--  -->
                            </div>
                        </div>
                    <!-- /.box-body -->


                <!-- end modal-body -->
                </div>
                <div class="modal-footer">
                    
                    <button type="submit" class="btn btn-primary" id="">Enregistrer</button>
                    <!-- data-dismiss="modal" cd code sert à dissimulé le modal -->
                </div>
            <!-- /form -->
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
{{--
    @section('javascript')

<script>


    $(document).ready(function(){

        //Initialize Select2 Elements
    })
</script>

@stop

--}}