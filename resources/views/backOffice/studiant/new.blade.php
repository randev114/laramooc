<!-- /.box-header -->
<div class="modal fade" id="modal-studiant">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form-studiant" method="post" action="{{ route('admin.studiant.add') }}">
            <!-- form -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-shadow">New studiant</h4>
                </div>
                <div class="modal-body">
                    <!-- form start -->

                        {{ csrf_field() }}
                        
                        <div class="box-body">
                            <div class="premary-content">
                                <!--  -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="select">
                                            <div class="form-group">
                                                <div>
                                                    
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--  -->
                                <!--  -->
                                <div class="row content-list">
                                    <div class="col-md-6">
                                        <h2>{{ ucfirst($person_form['personal_infos']) }}</h2>

                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['name']) }}</label>
                                            <input value="{{ Session::pull('person.name') }}" type="text" name="person[name]" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['prename']) }}</label>
                                            <input value="" type="text" name="person[firstname]" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['email']) }}</label>
                                            <input value="" type="text" name="person[email]" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['nationality']) }}</label>
                                            <input value=""  type="text" name="person[nationality]" class="form-control" autocomplete="off">
                                            
                                        </div>

                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['birthday']) }}</label>
                                            <input value="" id="birthday" type="" name="person[birthday]" class="form-control" autocomplete="off">
                                            <div style="position: relative; ">
                                                <div id="birthDayContainer" style="position: absolute;"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['sex']['label']) }}</label>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="radio">
                                                        <input id="sex-homme" type="radio" name="person[sex]" selected=true value=1>
                                                        <span>{{ ucfirst($person_form['sex']['mal']) }}</span>
                                                    </label>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="radio">
                                                        <input id="sex-femme" type="radio" name="person[sex]" class="" value="2">
                                                        <span>{{ ucfirst($person_form['sex']['femal']) }}</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['country']) }}</label>
                                            <input type="text" name="person[country]" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="content">{{ucfirst($person_form['city']) }}</label>
                                            <input type="" name="person[city]" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['phone_number']) }}</label>
                                            <input type="" name="person[phone_number]" class="form-control">
                                        </div> 
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['email_application']) }}</label>
                                            <select name="person[email_application]" class="form-control">
                                                <option value="">Choisir la</option>
                                                <?php foreach (config('datamiror.email_application') as $key => $email_application):?>

                                                    <option value="{{ $key }}">{{ $email_application }}</option>

                                                <?php endforeach; ?>

                                            </select>
                                        </div> 
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['user_name_email_application']) }}</label>
                                            <input type="" name="person[user_name_email_application]" class="form-control">
                                        </div> 
                                    </div>
                                    <div class="col-md-6">
                                        <h2>{{ $person_form['degree_level']['title_section'] }}</h2>
                                        
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['degree_level']['test_name']) }}</label>
                                            <select name="degree_level[test_name]" class="form-control">
                                                <option value="">Choisir la</option>
                                                <?php foreach (config('datamiror.degree_level') as $key => $degree_level):?>
                                                    <option value="{{ $key }}">{{ $degree_level }}</option>
                                                <?php endforeach;?>
                                            </select>
                                        </div> 
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['degree_level']['address1']) }}</label>
                                            <input type="" name="degree_level[address1]" class="form-control">
                                        </div> 
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['degree_level']['address2']) }}</label>
                                            <input type="" name="degree_level[address2]" class="form-control">
                                        </div> 
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['degree_level']['city']) }}</label>
                                            <input type="" name="degree_level[city]" class="form-control">
                                        </div> 
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['degree_level']['state_province']) }}</label>
                                            <input type="" name="degree_level[state_province]" class="form-control">
                                        </div> 
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['degree_level']['postal_code']) }}</label>
                                            <input type="" name="degree_level[postalcode]" class="form-control">
                                        </div> 
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['degree_level']['country']) }}</label>
                                            <input type="" name="degree_level[country]" class="form-control">
                                        </div> 
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['degree_level']['comment_or_questions']) }}</label>
                                            <textarea name="degree_level[comment_or_questions]" class="form-control textarea"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <!--  -->
                            </div>
                        </div>
                    <!-- /.box-body -->


                <!-- end modal-body -->
                </div>
                <div class="modal-footer">
                    
                    <button type="submit" class="btn btn-primary" id="save-studiant">Enregistrer</button>
                    <!-- data-dismiss="modal" cd code sert à dissimulé le modal -->
                </div>
            <!-- /form -->
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<style>
.radio {
  margin: 16px 0;
  display: block;
  cursor: pointer;
}
.radio input {
  display: none;
}
.radio input + span {
  line-height: 22px;
  height: 22px;
  padding-left: 22px;
  display: block;
  position: relative;
}
.radio input + span:not(:empty) {
  padding-left: 30px;
}
.radio input + span:before, .radio input + span:after {
  content: '';
  width: 22px;
  height: 22px;
  display: block;
  border-radius: 50%;
  left: 0;
  top: 0;
  position: absolute;
}
.radio input + span:before {
  background: #D1D7E3;
  transition: background 0.2s ease, transform 0.4s cubic-bezier(0.175, 0.885, 0.32, 2);
}
.radio input + span:after {
  background: #fff;
  transform: scale(0.78);
  transition: transform 0.6s cubic-bezier(0.175, 0.885, 0.32, 1.4);
}
.radio input:checked + span:before {
  transform: scale(1.04);
  background: #5D9BFB;
}
.radio input:checked + span:after {
  transform: scale(0.4);
  transition: transform 0.3s ease;
}
.radio:hover input + span:before {
  transform: scale(0.92);
}
.radio:hover input + span:after {
  transform: scale(0.74);
}
.radio:hover input:checked + span:after {
  transform: scale(0.4);
}

.radio-inline .radio {
    /* float:left; */
}
.textarea {
    max-width: 100%;
    height: 100%;
    min-height:205px;
}

</style>