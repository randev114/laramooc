
@extends('layouts.backOffice.adminLTE')

@section('content')



<section class="content-header">
    <h1>
        {{ config('app.name') }}
        <small>Recherche</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        {{--
            <li><a href="#">Forms</a></li>
        --}}
        <li class="active">home</li>
    </ol>
</section>


@include('backOffice.studiant.new')

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            {!! $errors->first('email', \App\Helpers\Alert::show(':message','danger')) !!}
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">Studiants</h3>
                    <button class="btn btn-info" id="new-studiant"> <i class="fa fa-plus"></i></button>
                </div>
                   
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div><div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example2" class="table table-bordered table-hover dataTable" studiant="grid" aria-describedby="example2_info">
                                    <thead>
                                        <tr studiant="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                            id</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">nom</th>
                                            <th colspan="2">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>


                                        @foreach($studiants as $key => $studiant)
                                            <tr studiant="row" class="odd">
                                              <td class="sorting_1">{{ $studiant->id }}</td>
                                              <td>{{ $studiant->name }}</td>
                                                <td> <a href="{{ route('admin.studiant.edit', $studiant->id) }}" class="btn">Edit</a></td>

                                                <td >
                                                    <form method="post" action="{{ route('admin.studiant.delete', $studiant->id )  }}">
                                                         {{ csrf_field() }}
                                                        
                                                        <button onClick="return confirm('Voulez-vous vraiment le supprimer ?')" type="submit" class="btn btn-danger">delete</button>
                                                    </form>
                                                    
                                                </td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                    
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-12" style="margin-left:15px;">
                                    {{ $studiants->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </div>
</section>

@stop

@section('javascript')
<script type="text/javascript">
    
    $(document).on('click', "#new-studiant", function(event){
        event.preventDefault();
        $('#modal-studiant').modal('show');
    });

    $("#birthday").datepicker({ 
        autoclose: true,
        inline: true,
        format: "yyyy/mm/dd",
        // container: "#birthDayContainer",
        autoPick: true,
        zIndex: 1062,
        left:0
    });

    $(document).on("click", "#save-studiant", function (event){

        event.preventDefault();

        var form = $("#form-studiant");
        var datas_form = $(form).serialize();

        $.ajax({
            url : $(form).attr('action'),
            data: datas_form,
            dataType : "json",
            method : "POST",
            success : function (response) {
                console.log(response);
                $("#modal-studiant").modal("hide");
                
            }
        });
    });


    $(document).ready(function() {
        $("#sex-homme").prop("checked", true);
    });

</script>
<!-- <script src="{{ asset('js/page.js') }}"></script> -->
<script src="{{ asset('js/script.js') }}" type="text/javascript"></script>
<script type="text/javascript">


$('#search-page').script();

/**
--------------------------------------------------------------------------------


/**
---------------------------------------------------------------------------------
 */
//Date range picker
$('#reservation').daterangepicker()
    //Date range picker with time picker
$('#reservationtime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        format: 'MM/DD/YYYY h:mm A'
    })

//Date range as a button
    $('#daterange-btn').daterangepicker({
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: moment().subtract(29, 'days'),
            endDate: moment()
        },
        function(start, end) {
            $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
        }
    )

//Date picker
$('#datepicker').datepicker({
    autoclose: true
})

// alert(this);

//



</script>
@stop