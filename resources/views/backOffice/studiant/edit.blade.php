
@extends('layouts.backOffice.adminLTE')

@push('stylesheet')
  <link href=" {{ asset('css/form-radio.css') }} " rel="stylesheet" />
@endpush

@section('content')

<section class="content">
    <div class="row">
        <div class="col-xs-12">
			{!! $errors->first('email', \App\Helpers\Alert::show(':message','danger')) !!}
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">Studiants</h3>
                   
                </div>
                   
                <!-- /.box-header -->
                <div class="box-body">
		            <form method="post" action="{{ route('admin.studiant.save_edit', $studiant->id) }}">
		            <!-- form -->

		                    <!-- form start -->

		                        {{ csrf_field() }}
		                        
		                        <div class="box-body">
		                            <div class="premary-content">
		                                <!--  -->
		                                <div class="row">
		                                    <div class="col-md-12">
		                                        <div class="select">
		                                            <div class="form-group">
		                                                <div>
		                                                    
		                                                </div>
		                                            </div>
		                                        </div>

		                                    </div>
		                                </div>
		                                <!--  -->
		                                <!--  -->
		                                <div class="row content-list">

		                                    <div class="col-md-6">
		                                    	<h2>{{ ucfirst($person_form['personal_infos']) }}</h2>

		                                    	<input type="hidden" name="person[id]" class="form-control" value="{{ $studiant->uid }} ">


		                                        <div class="form-group">
		                                           <label for="content">{{ ucfirst($person_form['name']) }}</label>
		                                            <input type="" name="person[name]" class="form-control" value="{{ $studiant->name }} ">
		                                        </div>
		                                        <div class="form-group">
		                                           <label for="content">{{ ucfirst($person_form['prename']) }}</label>
		                                            <input type="" name="person[firstname]" class="form-control" value="{{ $studiant->firstname }} ">
		                                        </div>

		                                        <div class="form-group required">
		                                             <label for="content">{{ ucfirst($person_form['email']) }}</label>
		                                            <input type="" name="person[email]" class="form-control" value="{{ $studiant->email }} ">
		                                        </div>
		                                         <div class="form-group">
		                                            <label for="content">{{ ucfirst($person_form['nationality']) }}</label>
		                                            <input value="{{ $studiant->nationality }}"  type="text" name="person[nationality]" class="form-control" autocomplete="off">
		                                            
		                                        </div>
		                                        <div class="form-group">
		                                            <label for="content">Birthday</label>
		                                            <input id="birthday" type="" name="person[birthday]" class="form-control" autocomplete="off" value="{{ $studiant->birthday }} ">
		                                            <div style="position: relative; ">
		                                                <div id="birthDayContainer" style="position: absolute;"></div>
		                                            </div>
		                    
		                                        </div>
		                                       	<div class="form-group">
		                                            <label for="content">{{ ucfirst($person_form['sex']['label']) }}</label>
		                                            <div class="row">
		                                                <div class="col-md-6" id="sex_radio" data-sex="{{ $studiant->sex }}">
		                                                    <label class="radio">
		                                                        <input id="sex-homme" type="radio" name="person[sex]"  value="1" >
		                                                        <span>{{ ucfirst($person_form['sex']['mal']) }}</span>
		                                                    </label>
		                                                </div>
		                                                <div class="col-md-6">
		                                                    <label class="radio">
		                                                        <input id="sex-femme" type="radio" name="person[sex]" class="" value="2">
		                                                        <span>{{ ucfirst($person_form['sex']['femal']) }}</span>
		                                                    </label>
		                                                </div>
		                                            </div>
		                                        </div>
		                                        <div class="form-group">
		                                            <label for="content">Country</label>
		                                            <input type="" name="person[country]" class="form-control" value="{{ $studiant->country }} ">
		                                        </div>
		                                        <div class="form-group">
		                                            <label for="content" >City</label>
		                                            <input type="" name="person[city]" class="form-control" value="{{ $studiant->city }} " >
		                                        </div>

		                                    </div>
		                                    <div class="col-md-6">
		                                        <h2>{{ $person_form['degree_level']['title_section'] }}</h2>
		                                        
		                                        <div class="form-group">
		                                            <label for="content">{{ ucfirst($person_form['degree_level']['test_name']) }}</label>
		                                            <select name="degree_level[test_name]" class="form-control">
		                                                <option value="">Choisir la</option>
		                                                <?php foreach (config('datamiror.degree_level') as $key => $degree_level):?>
		                                                	@if ($studiant['test_name'] == $key)
															<option selected="true" value="{{ $key }}">{{ $degree_level }}</option>
															@else
															<option value="{{ $key }}">{{ $degree_level }}</option>
															@endif
		                                                <?php endforeach;?>
		                                            </select>
		                                        </div> 
		                                         <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['degree_level']['address1']) }}</label>
                                            <input value="{{ $studiant['address1'] }}" type="" name="degree_level[address1]" class="form-control">
                                        </div> 
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['degree_level']['address2']) }}</label>
                                            <input value="{{ $studiant['address2'] }}" type="" name="degree_level[address2]" class="form-control">
                                        </div> 
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['degree_level']['city']) }}</label>
                                            <input value="{{ $studiant['city'] }}" type="" name="degree_level[city]" class="form-control">
                                        </div> 
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['degree_level']['state_province']) }}</label>
                                            <input value="{{ $studiant['state_province'] }}" type="" name="degree_level[state_province]" class="form-control">
                                        </div> 
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['degree_level']['postal_code']) }}</label>
                                            <input value="{{ $studiant['postalcode'] }}" type="" name="degree_level[postalcode]" class="form-control">
                                        </div> 
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['degree_level']['country']) }}</label>
                                            <input value="{{ $studiant['country'] }}" type="" name="degree_level[country]" class="form-control">
                                        </div> 
                                        <div class="form-group">
                                            <label for="content">{{ ucfirst($person_form['degree_level']['comment_or_questions']) }}</label>
                                            <textarea name="degree_level[comment_or_questions]" class="form-control textarea">{{ $studiant['comment_or_questions'] }}</textarea>
                                        </div>
		                                    </div>
		                                </div>

		                                <!--  -->
		                            </div>
		                        </div>
		                    <!-- /.box-body -->
		                <!-- end modal-body -->
		                </div>
		                <div class="modal-footer">
		                    
		                    <button type="submit" class="btn btn-primary" id="">Enregistrer</button>
		                    <!-- data-dismiss="modal" cd code sert à dissimulé le modal -->
		                </div>
		            <!-- /form -->
		            </form>
				</div>

			</div>
		</div>
	</div>
</section>

@stop

@section("javascript")

<script type="text/javascript">
	
	
	var sex = $('#sex_radio').data("sex");

	if (sex == 1)
	{
		 $("#sex-homme").prop("checked", true);
	}else 
		if (sex == 2)
		{
			 $("#sex-femme").prop("checked", true);
		}


</script>
@endsection