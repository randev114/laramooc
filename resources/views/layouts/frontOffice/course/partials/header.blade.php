    <!-- Header -->

    <header class="header d-flex flex-row">
        <div class="header_content d-flex flex-row align-items-center">
            <!-- Logo -->
            <div class="logo_container">
                <div class="logo">
                    <img src="{{ asset('public/fronts/images/logo.png') }}" alt="">
                    <span>UPEI</span>
                </div>
            </div>

            <!-- Main Navigation -->
            <nav class="main_nav_container">
                <div class="main_nav">
                    <ul class="main_nav_list">
                        <li class="main_nav_item"><a href="{{ route('public.home') }}">home</a></li>
                        <li class="main_nav_item"><a href="{{ route('public.course.list') }}">about us</a></li>
                        <li class="main_nav_item"><a href="{{ route('public.course.list') }}">courses</a></li>
                        <li class="main_nav_item"><a href="#">elements</a></li>
                        <li class="main_nav_item"><a href="{{ route('public.news') }}">news</a></li>
                        <li class="main_nav_item"><a href="{{ route('public.contact') }}">contact</a></li>
                        <li class="main_nav_item"><a href="{{ route('login') }}">login</a></li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="header_side d-flex flex-row justify-content-center align-items-center">
            <img src="{{ asset('public/fronts/images/phone-call.svg') }}" alt="">
            <span>+43 4566 7788 2457</span>
        </div>

        <!-- Hamburger -->
        <div class="hamburger_container">
            <i class="fas fa-bars trans_200"></i>
        </div>

    </header>