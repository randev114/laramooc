
        
<?php
    // root est la racine de notre css courrant
    $root = 'adminLTE';
?>

@include('layouts.backOffice.template_test.partials.header')

@include('layouts.backOffice.template_test.partials.left')


<div class="content-wrapper">
    @yield('content')
</div>

@include('layouts.backOffice.template_test.partials.footer')