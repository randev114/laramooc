<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset($root.'/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ auth()->user()->person()->first() }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            @role('superadmin|admin|teacher|etudiant')
                <li>
                    <a href="{{ route('backOffice.dashboard') }}">
                        <i class="fa fa-th"></i> <span>Dashboard</span>
                        <span class="pull-right-container">
                            <small class="label pull-right bg-green">Initialisation</small>
                        </span>
                    </a>
                </li>
            @endrole
            @role('superadmin|admin')
            <li>
                <a href="{{ route('bo.prof.matter.list') }}">
                    <i class="fa fa-th"></i> <span>Matière</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-green">0</small>
                    </span>
                </a>
            </li>
            @endrole

            @role('teacher|admin|superadmin')
            @else

                <li>
                    <a href="{{ route('public.cours.list') }}">
                        <i class="fa fa-th"></i> <span>Course</span>
                        <span class="pull-right-container">
                            <small class="label pull-right bg-green">0</small>
                        </span>
                    </a>
                </li>
            @endrole

            @role('admin')

            <li class="treeview">
                    <a href="#">
                        <i class="fa fa-book"></i>
                        <span>Cours</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('bo.admin.manager') }}"><i class="fa fa-circle-o"></i> gestion des coures</a></li>
                    <li>
                        <a href="{{ route('bo.admin.course.index') }}">
                            <i class="fa fa-circle-o"></i> Assigner un cours
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.course.list') }}">
                            <i class="fa fa-th"></i> <span>CRUD Course</span>
                            <span class="pull-right-container">
                                <small class="label pull-right bg-green">0</small>
                            </span>
                        </a>
                    </li>

                    <li><a href="charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
                    <li><a href="charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
                </ul>
            </li>

            <li>
                <a href="{{ route('superadmin.assets.typeList') }}">
                    <i class="fa fa-th"></i> <span>CRUD assets type</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-green">0</small>
                    </span>
                </a>
            </li>

            @endrole
            @role('teacher')

            <li>
                <a href="{{ route('bo.prof.course.list') }}">
                    <i class="fa fa-th"></i> <span>Course</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-green">0</small>
                    </span>
                </a>
            </li>
                @elserole('admin|teacher')

            @endrole
            @role('superadmin|admin')
                {{-- manager.studiant.user --}}
            <li>
                <a href="{{ route('manager.studiant.user') }}">
                    <i class="fa fa-th"></i> <span>Studiant</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-green">Initialisation</small>
                    </span>
                </a>
            </li>
            @endrole
            @role('superadmin|admin')
            <li>
                <a href="{{ route('admin.teacher.list') }}">
                    <i class="fa fa-th"></i> <span>Teacher</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-green">Initialisation</small>
                    </span>
                </a>
            </li>
                @else

                @endrole
            @role('superadmin|admin|etudiant')
            <li>
                <a href="{{ route('auth.forum.index') }}">
                    <i class="fa fa-th"></i> <span>Forum</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-green">Initialisation</small>
                    </span>
                </a>
            </li>
                @else

                @endrole
            @role('superadmin|admin')
            <li>
                <a href="{{ route('admin.permission.list') }}">
                    <i class="fa fa-th"></i> <span>Permission</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-green">2</small>
                    </span>
                </a>
            </li>
            @endrole

            @role('superadmin|admin')
            <li>
                <a href="{{ route('admin.roles.list') }}">
                    <i class="fa fa-calendar"></i> <span>Roles</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-red">3</small>
                        <small class="label pull-right bg-blue">17</small>
                    </span>
                </a>
            </li>
            @endrole

            @role('superadmin|admin')
                <li>
                    <a href="{{ route('admin.users.index') }}">
                        <i class="fa fa-calendar"></i> <span>Utilisateurs</span>
                        <span class="pull-right-container">
                            <small class="label pull-right bg-red">3</small>
                            <small class="label pull-right bg-blue">17</small>
                        </span>
                    </a>
                </li>
            @endrole
            @role('teacher|admin')

            @endrole
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
