<?php


return [
    "person" =>
        [
            "fr" => [
                "page_title"        => "Nouvelle utilisateur",
                "personal_infos"    => "Information personnels",
                "name"              => "nom",
                "password"          => "Mot de passe",
                "prename"           => "prénom",
                "email"             => "email",
                "adress"            => "Adresse",
                "birthday"          => "Date de naissance",
                "country"           => "Pays",
                "city"              => "Ville",
                "nationality"       => "Nationalité",
                "sex"               => ['label'=> "Sexe", "mal"=>"homme", "femal"=> "femme"],

                "phone_number"                  => "Numéro de téléphone",
                "email_application"             => "Appli de messagerie",
                "user_name_email_application"   => "Nom d'utilisateur de l'appli de messagerie",
                "degree_level"      => 
                    [
                        "title_section"         => "Niveau du diplôme désiré",
                        "test_name"             =>  "Nom du test",
                        "address1"              =>  "Adresse1",
                        "address2"              =>  "Adresse2",
                        "city"                  =>  "Ville",
                        "state_province"        =>  "Etat/Province",
                        "postal_code"           => "Code Postal",
                        "country"               => "Pays",
                        "comment_or_questions" 	=> "Autres commentaires ou questions"
                    ],
                    "teacher" =>
                    [
                        "matter"            => "matière",
                        "class"             => "classe",
                    ]
            ]
        ],



];



?>