<?php 

return [

		"degree_level" => [
			1 => 'TOEFEL',
			2 => 'IELTS',
			3 => 'Pearson',
			4 => 'ACT Eng',
		],
		"email_application" => 
		[
			1 => 'Skype',
			2 => 'WhatsApp',
			3 => 'Viber',
			4 => 'Google Hangouts',
		],
		"matters" => 
		[
			1 => "Computer science",
			2 => "Animal Biology"
		], 
		"class" => 
		[
			1 => "1 ère anné",
			2 => "2 eme années",
			3 => "3 eme années",
			4 => "4 eme années",
			5 => "5 eme années",
		]
		,"people" =>
		[
			\App\Models\Person::TEACHER => "Teacher",
			\App\Models\Person::STUDIANT => "Etudiant",
		]

];

?>