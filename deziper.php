<?php


$zip = "laramooc";

$file_name = "../{$zip}.zip";
// unzip out dir
$destination = dirname(__FILE__)."/../";


// if dir not exist
if (!file_exists($destination.$zip))
{
	zip_extract($file_name, $destination );
}


function zip_extract($file, $extractPath) {

    $zip = new ZipArchive;
    $res = $zip->open($file);
    if ($res === TRUE) {
        $zip->extractTo($extractPath);
        $zip->close();
        return TRUE;
    } else {
        return FALSE;
    }

}


?>