

2019-11-09
----------------

# CreateImageTable: 

create table `images` 
	(
	`id` bigint unsigned not null auto_increment primary key,
	`name` varchar(255) null, 
	`cripted_name` varchar(255) null,
	`size` int null, 
	`course_id` int null, 
	`teacher_id` int null, 
	`type` int null, 
	`created_at` timestamp null, 
	`updated_at` timestamp null
	) 
	default character set utf8 collate 'utf8_unicode_ci'