
2019-11-13 (November)
----------------
# Add columns into courses
/*****************************************************/
`ALTER TABLE
    courses ADD COLUMN `user_id` INT(11) NULL,
    ADD COLUMN `is_active` INT(11) NULL DEFAULT NULL COMMENT '1 yes/ 0=no',
    ADD COLUMN `short_desc` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    ADD COLUMN `long_desc` TEXT COLLATE utf8_unicode_ci,
    ADD COLUMN `indication_admin` TEXT COLLATE utf8_unicode_ci,
    ADD COLUMN `date_debut` DATE DEFAULT NULL,
    ADD COLUMN `date_fin` DATE DEFAULT NULL,
    ADD COLUMN `heure_debut` TIMESTAMP NULL DEFAULT NULL,
    ADD COLUMN `heure_fin` TIMESTAMP NULL DEFAULT NULL,
    ADD COLUMN `course_type_id` INT(11) DEFAULT '0' COMMENT '0 = normal / 1 = stock',
    ADD COLUMN `assets_list_id` INT(11) DEFAULT NULL,
    ADD COLUMN `image` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    ADD COLUMN `has_chapter` INT(11) DEFAULT NULL,
    ADD COLUMN `chapter` INT(11) DEFAULT NULL;`
/*****************************************************/

    2019-11-14
--------------------

# delete columns teacher_id from courses

pphp artisan migrate:reset --path=/database/migrations/2019_11_07_091338_add_column_teacher_id_into_tabl