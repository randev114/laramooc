<?php
namespace App\Helpers;

class Alert
{

    public static function show ($message, $type = 'success')
    {
        $html = '<div class="alert alert-'.$type.' alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>';
        
        return $html .= $message.'</div>';
    }
}