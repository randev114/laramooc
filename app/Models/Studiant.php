<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Studiant extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'prename',
        'email',
        'adress',
        'postal_code',
        'created_at',
        'country',
    ];


}
