<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignmentCourses extends Model
{
    protected $table = "assignment_courses";

    protected $fillable = [
    						'course_id',
    						'user_id'
    					];
}
