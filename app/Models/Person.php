<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    public const SUPERADMIN   = 1;
    public const ADMIN        = 2;
    public const TEACHER      = 3;
    public const STUDIANT     = 4;

    protected $fillable = 
    [
        "user_type",
        "sex",
        "name",
        "firstname",
        "email",
        "nationality",
        "birthday",
        "country",
        "city",
        "postalcode",
        "phone_number",
        "email_application",
        "user_name_email_application",
    ];

    /**
     * Get the user that owns the phone.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
