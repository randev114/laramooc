<?php

namespace App\Http\Controllers\BackOffice;

use Spatie\Permission\Models\Role;
use App\User;
use App\Models\Person;
use App\Models\Teacher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use DB;
class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        Person::TEACHER;

        $teachers = Person::select("people.*")
              ->where('user_type',"=", Person::TEACHER)
              ->orderBy('id','desc')
              ->paginate(5);


        // $teachers = User::select("*","model_has_roles.role_id")
        //             ->join("model_has_roles", "users.id", "model_has_roles.model_id")
        //             ->join("roles", "model_has_roles.role_id", "roles.id")
                    
        //             ->where('roles.name',"=","teacher")->orderBy('users.id','desc')->paginate(5);



        $person_form = config('form.person');

        return view("backOffice.teacher.index",[
            "teachers" => $teachers,
            "person_form" => $person_form['fr']
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $person     = $request->get('person');
        $teacher    = $request->get('teacher');

            $person = array_merge($person, [
                "user_type" => Person::TEACHER,
            ]);


        $rules = [
            'name' => ['required'],
            'email'=>['required','regex:/(.+)@[a-zA-Z]{3,}\.[a-zA-Z]{2,3}/i']
          ];

        $messages = ["email.regex"=>"L'email est invalide"];

        $isValid = Validator::make($person, $rules, $messages)->validate();

        $teacher_created = Person::where(["email" => $person['email']])->first();



        

      if ($teacher_created == null)
        {
            DB::transaction(function () use ($person, $teacher) {

                DB::table('people')->insert($person); // insertion into the database

                $id = DB::getPdo()->lastInsertId();

                DB::table('teachers')->insert([
                    "person_id"     => $id,
                    "matter"        => $teacher['matter_id'],
                    "class"         => $teacher['class_id'],
                ]);

            });
        }

        return redirect()->route('admin.teacher.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
