<?php

namespace App\Http\Controllers\BackOffice;

use App\User;
use App\Models\Person;
use App\Models\DegreeLevel;
use Spatie\Permission\Models\Role;
use App\Models\ServicesModels\RoleServices;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;

class StudiantController extends Controller
{

  public function __construct()
  {
    $this->roleServices = new \App\Models\ServicesModels\RoleServices();
  }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Person::STUDIANT;

        $studiants = Person::select("people.*")
                      ->where('user_type',"=", Person::STUDIANT)
                      ->orderBy('id','desc')
                      ->paginate(5);

                    // ->orderBy('users.id','desc');
        $person_form = config('form.person');


        return view("backOffice.studiant.index",[
            "studiants"   => $studiants,
            "person_form" => $person_form['fr'] ,
        ]);
    }



    public function store(Request $request) 
    {

      $json_message = ["message" => "Il n'y a pas une enregistrement. "];

      $request = $request->all();

      $studiant     = $request['person'];
      $degree_level = $request['degree_level'];

      $rules = [
            'firstname' => ['required'],
            'email'=>['required','regex:/(.+)@[a-zA-Z]{3,}\.[a-zA-Z]{2,3}/i']
          ];

      $messages = ["email.regex"=>"L'email est invalide"];

      $isValid = Validator::make($studiant, $rules, $messages)->validate();

      $studiant_created = Person::where(["email" => $studiant['email']])->first();



      if ($studiant_created == null)
      {
        $sexAndEmail_application_toIntval  = 
        [
          "sex"               => intval($studiant["sex"]),
          "email_application" => intval($studiant["email_application"]),
          "user_type" => Person::STUDIANT
        ];

        $studiant = array_merge($studiant , $sexAndEmail_application_toIntval);

        // dd($studiant);

        $person = Person::create($studiant);

        Session::put("person.studiant_id", $person->id);
        Session::save();

        if ($person->id)
        {
          DegreeLevel::create(array_merge($degree_level, ["studiant_id" => $person->id]));
        }

        $json_message = ["message" => "Enregistrement éffectué avec succés"];
      } 

      // if (DegreeLevel::where(["studiant_id" => $studiant_created->id]) == null)
      // {
      //   DegreeLevel::create(array_merge($degree_level, ["studiant_id" => $studiant_created->id]));
      //   $json_message = ["message" => "Enregistrement éffectué avec succés"];
      // }

      return new Response(json_encode($json_message));

    }


    public function delete ($id)
    {
      $user = User::where(["id" => $id])->first();

      $person_id = $user->person_id;

      $user->delete();

      Person::where(["id" => $person_id])->first()->delete();

      return redirect()->route('manager.studiant.user');
    }



    /**
     * Edition of the person
     */
    public function edit ($id)
    {

      $studiant = Person::select("people.*","degree_levels.*","people.id as uid")
                  ->leftjoin("degree_levels", "degree_levels.studiant_id", "people.id")
                  ->first();

      // dd($studiant->toArray() );

      $person_form = config('form.person');

      return view("backOffice.studiant.edit",[
          "studiant"    => $studiant,
          "person_form" => $person_form["fr"],
      ]);
    }

    /**
     * Store data after editing the posted
     */
    public function save_edit ($id, Request $request)
    {

      $person_from_request       = $request->get('person');
      $degree_level_from_request = $request->get('degree_level');
      
      $rules = [
          'firstname' => ['required'],
          'email'=>['required','regex:/(.+)@[a-zA-Z]{3,}\.[a-zA-Z]{2,3}/i']
        ];

      $validator = Validator::make($person_from_request, $rules)->validate();

      $studiant   = Person::where(["id" => intval( $person_from_request['id'])])->first();
      $level      = DegreeLevel::where(["studiant_id" => intval( $person_from_request['id'])])->first();

      $studiant = $studiant->toArray();
      $level    = $level->toArray();
      
      if (isset($studiant['created_at']))
        unset($studiant['created_at']);

      if (isset($studiant['updated_at']))
        unset($studiant['updated_at']);

      if (isset($level['created_at']))
        unset($level['created_at']);

      if (isset($level['updated_at']))
        unset($level['updated_at']);

      $diff       = array_diff($person_from_request, $studiant);
      $diff_level = array_diff( $degree_level_from_request, $level);
      
      dump($diff);
      dump($diff_level);
   

      exit;

      if (!empty ($diff)) 
      {
        if (isset($diff['postalcode']))
        {
           $diff['postalcode'] = substr($diff['postalcode'], 0, 5);
        }
         Person::where('id', $id)->update($diff);
      }

      return redirect()->route("manager.studiant.user");
    }
}
