<?php

namespace App\Http\Controllers\BackOffice;
use App\Models\Role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function manager()
    {
		$roles = Role::paginate(4);

		// exit;
		foreach ($roles->toArray() as $key => $role) {
			// dump($role);
		}
		
		return view("backOffice.dashboard.manager",[
			"roles" => $roles
		]);
    }
}
