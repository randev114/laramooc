<?php

namespace App\Http\Controllers\BackOffice;

use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use App\Models\ServicesModels;

class RolesController extends Controller
{
	protected $roleServices;

	public function __construct()
	{
		$this->roleServices = new \App\Models\ServicesModels\RoleServices();
	}

	public function index()
	{
		// $role = Role::create(['name' => 'writer']);
		// $permission = Permission::create(['name' => 'write articles']);

		// $role = Role::findById(1);

		// $role = Role::create(['name' => 'prof']);
		// $permission = Permission::findById(2);
		// $role->givePermissionTo($permission);
		// $permission->removeRole($role);

		// auth()->user()->givePermissionTo('write articles');
		// $role = Role::create(['name' => 'teacher']);
		// auth()->user()->assignRole('teacher');

		// return auth()->user()->permissions;



		return view("backOffice.roles.list");
	}
	
	public function manage()
	{

		$roles = Role::paginate(4);
		
		return view("backOffice.roles.list",[
			"roles" => $roles
		]);
	}

	public function assignment(Request $request)
	{	

		$datas = $request->all();

		$user_to_assigned = $datas['user_to_assigned'];

		$this->roleServices->checkMissUserRole($user_to_assigned, $request);
		$this->roleServices->putNewUserRolesRemoveOlds();
		// exit;

		// exit;
		// // $items = $this->roleServices->findUserAllRole($user_to_assigned);

		// $user = User::find(['id' => $user_to_assigned])->first();

		// foreach ($datas['role'] as $key => $role_id) {
			
		// 	$role = Role::findById($role_id);

		// 	if(! $user->hasRole($role->name))
		// 	{
		// 		$user->assignRole($role->name);
		// 	}
		// }

		return redirect()->route('admin.users.index');
	}

	public function UserHasRoles($id)
	{

		$items = $this->roleServices->findUserAllRole($id);

		return new Response(json_encode($items));
		
	}

}
?>