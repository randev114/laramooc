<?php

namespace App\Http\Controllers\BackOffice\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Datetime;
use DB;
class CourseAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = \App\Models\Course::paginate(5);

        $columns = [
                        'c.id',
                        't.id as tid',
                        'c.title',
                        't.name'
                    ];

        $courses = \App\Models\Course::from('courses as c')
            ->select($columns)
            ->leftjoin('assignment_courses as a_c', 'a_c.course_id','c.id')
            ->leftjoin('users as t', 't.id', 'a_c.user_id')
            ->leftjoin('assignment_courses as ac', 'ac.user_id','t.id')
            // ->where([
            //     'r.name' => 'teacher'
            //     ])

            ->paginate(5)
           ;


       $columns = [
                    'u.person_id',
                    'u.id as uid',
                    'u.name',
                    'u.email',
                    'r.name as role_name',
                    'ac.user_id'
                ];

        $teachers = \App\User::from('users as u')
            ->select($columns)
            ->leftjoin('model_has_roles as m_r','m_r.model_id','u.id')
            ->leftjoin('roles as r', 'r.id','m_r.role_id')
            ->leftjoin('assignment_courses as ac', 'ac.user_id','u.id')

            ->where([
                'r.name' => 'teacher'
                ])
            ->where('ac.user_id','=',null)

            ->get()

           ;


        $person_form = config('form.person');

        return view('backOffice.course.admin.index', [
            "courses"       => $courses,
            "person_form"   => $person_form['fr'] ,
            "teachers"      => $teachers,
        ]);
    }

    public function manager()
    {
        $courses = \App\Models\Course::from('courses as c')
            ->select('c.*')
            ->paginate(10)
            // ->toArray()
            ;
        // dd($courses['data']);

        return view('backOffice.course.admin.manager',[
            'courses'   => $courses,
        ]);
    }

    public function actions(Request $request, $id)
    {
        $columns = [
                        'u.person_id',
                        'u.id as uid',
                        'u.name',
                        'u.email',
                        'r.name as role_name'
                    ];

        $teachers = \App\User::from('users as u')
            ->select($columns)
            ->leftjoin('model_has_roles as m_r','m_r.model_id','u.id')
            ->leftjoin('roles as r', 'r.id','m_r.role_id')
            ->leftjoin('assignment_courses as ac', 'ac.user_id','u.id')

            // ->where([
            //     'r.name' => 'teacher'
            //     ])
            // ->where('u.id','=','ac.user_id')

            ->get()
           ;


        $course = \App\Models\Course::where(['id' => $id])->first();

        return view('backOffice.course.admin.actions',[
            'id'        => $id,
            'course'    =>  $course,
            'teachers'  => $teachers
        ]);
    }




	public function savecourse(Request $request)
	{
		request()->validate([

            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

		$fields = $request->get('course');

		if (!isset($fields['matter_id']))
		{
			$fields['matter_id'] = 1;
        }

		$user_id = $fields['user_id'];
        $path = public_path('uploaded/course_assets/image/').$user_id;
        $path_url = "uploaded/course_assets/image/".$user_id;
        \App\Helpers\Dir::create_dir(explode('/', $path_url));

        $image_file = request()->image;

        if($image_file)
        {
            $new_file_name = rand().'.'.$image_file->getClientOriginalExtension();
            $image_file->move($path, $new_file_name);

            $image = $path_url.'/'.$new_file_name;

            $fields['image'] = $image;
        }

        $image_course = \App\Models\Assets::create([
            $path.'/'. $new_file_name
        ]);

        // dd($image_course);

		$dt = new \DateTime();
        $fields['created_at'] = $dt->format('Y-m-d H:i:s');

        $id_assets = $fields['assets_list_id'];

        if(isset($fields['teacher_id']))
        {
            unset($fields['teacher_id']);
        }



        $id_course = DB::table('courses')->insertGetId($fields);



        if(\App\Models\AssocAssetsCourse::where(['id_assets'=> $id_assets])->first())
        {
            // DB::table('assoc_assets_course')
			// ->where('id_assets', $id_assets)
            // ->update(['id_course' => $id_course]);
        }else
            {
                \App\Models\AssocAssetsCourse::create([
                    'id_assets' => $id_assets,
                    'id_course' => $id_course,
                ]);
            }



        return redirect()->route('admin.course.list');

	}

    public function save_assignment(Request $request)
    {

        $assignment_course = $request->get('assignment_course');
        $course_id = $assignment_course['course_id'];

        // dd($assignment_course );

        if (isset($assignment_course['user_id']) && intval($assignment_course['user_id']) == 0)
        {
            return redirect()->route('bo.admin.course.actions', ['id'=> $course_id]);
        }

        \App\Models\AssignmentCourses::create($assignment_course);


        return redirect()->route('bo.admin.course.index');
    }

    public function rupture_assignation(Request $request)
    {
        $rupture = $request->get('rupture_course');

        \App\Models\AssignmentCourses::where([
            'course_id' => $rupture['course_id']
        ])
            ->delete();

        return redirect()->route('bo.admin.course.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
