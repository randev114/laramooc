<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Artisan;
use Stdclass;
use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    

    public function cropper ($id = null)
    {
    	$user = new User();

    	return view ("backOffice.cropper.profile",[
    		'user' => $user
    	]);
    }

    public function upload_file_cropped(Request $request)
    {
    	if (!defined("DS"))
    	{
    		define("DS", DIRECTORY_SEPARATOR);
    	}
    	// dump($request->files->get('avatar'));
    	$app_public_file = app('path.public');
    	$uploaded_database = "uploaded";
		
    	$object = new Stdclass();
    	$file = $request->files->get('avatar');


		$user_id = $request->input('_id', 13);
		
		Artisan::call('cache:clear');

    	$url_base = $uploaded_database."/{$user_id}/avatar.png";

    	$config_uploaded_file = \App\Helpers\Dir::create_dir(["uploaded",$user_id], $app_public_file);

    	if ($file->move($config_uploaded_file, 'avatar.png'))
    	{
    		$user = User::find(['id' => $user_id])->first();
    		$user->avatar = $url_base ;
    		$user->save();
    	}
    	
		return new Response(json_encode($object));

    	exit;
    }
}
