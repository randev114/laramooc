-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mer. 20 nov. 2019 à 09:00
-- Version du serveur :  10.1.33-MariaDB
-- Version de PHP :  7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `stripmooc`
--

-- --------------------------------------------------------

--
-- Structure de la table `assets`
--

CREATE TABLE `assets` (
  `id` int(11) NOT NULL,
  `type_id` int(5) NOT NULL COMMENT 'type of asssets (exvideo/pdf/...)',
  `title` varchar(255) NOT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `long_description` text,
  `assets_filename` varchar(255) DEFAULT NULL,
  `assets_fileurl` text,
  `image_1` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `assets_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `assets`
--

INSERT INTO `assets` (`id`, `type_id`, `title`, `short_description`, `long_description`, `assets_filename`, `assets_fileurl`, `image_1`, `updated_at`, `created_at`, `assets_user_id`) VALUES
(1, 1, 'Event 1', NULL, NULL, '2025669165.jpg', 'course_assets/10/video/2025669165.jpg', 'course_assets/10/video/978814164.png', NULL, '2019-11-13 12:30:25', 10),
(2, 1, 'Event 1', NULL, NULL, '1062844451.jpg', 'course_assets/10/video/1062844451.jpg', 'course_assets/10/video/1910365858.png', NULL, '2019-11-13 12:30:40', 10),
(3, 1, 'Event 1', NULL, NULL, '757242065.jpg', 'course_assets/10/video/757242065.jpg', 'course_assets/10/video/479087874.PNG', NULL, '2019-11-13 12:49:02', 10),
(4, 1, 'sdfsg', NULL, NULL, '462032952.PNG', 'course_assets/10/video/462032952.PNG', 'course_assets/10/video/1455295432.png', NULL, '2019-11-13 12:54:04', 10),
(5, 2, 'Jute un titre', NULL, NULL, '629468205.png', 'course_assets/11/image/629468205.png', 'course_assets/11/image/771281201.png', NULL, '2019-11-14 07:40:42', 11),
(6, 2, 'sdfsdf', NULL, NULL, '1358701572.png', 'course_assets/11/image/1358701572.png', 'course_assets/11/image/1576451824.png', NULL, '2019-11-14 07:42:07', 11),
(7, 2, 'Event 1', NULL, NULL, '1627000590.png', 'course_assets/11/image/1627000590.png', 'course_assets/11/image/1153349204.jpg', NULL, '2019-11-20 05:54:42', 11);

-- --------------------------------------------------------

--
-- Structure de la table `assets_types`
--

CREATE TABLE `assets_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT '0=no / 1=yes',
  `description` varchar(255) DEFAULT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `assets_types`
--

INSERT INTO `assets_types` (`id`, `name`, `is_active`, `description`, `updated_at`, `created_at`) VALUES
(1, 'video', 1, 'video', '2019-11-13', '2019-11-13'),
(2, 'image', 0, 'Testsdfsdf', '2019-11-14', '2019-11-14');

-- --------------------------------------------------------

--
-- Structure de la table `assignment_courses`
--

CREATE TABLE `assignment_courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `course_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `assignment_courses`
--

INSERT INTO `assignment_courses` (`id`, `course_id`, `user_id`, `created_at`, `updated_at`) VALUES
(4, 18, 12, '2019-11-15 11:08:58', '2019-11-15 11:08:58'),
(6, 17, 11, '2019-11-15 11:27:37', '2019-11-15 11:27:37'),
(9, 16, 5, '2019-11-18 03:45:21', '2019-11-18 03:45:21');

-- --------------------------------------------------------

--
-- Structure de la table `assoc_assets_course`
--

CREATE TABLE `assoc_assets_course` (
  `id` int(11) NOT NULL,
  `id_course` int(11) DEFAULT NULL,
  `id_assets` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `assoc_assets_course`
--

INSERT INTO `assoc_assets_course` (`id`, `id_course`, `id_assets`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, '2019-11-13 12:30:25', NULL),
(2, NULL, 2, '2019-11-13 12:30:40', NULL),
(3, NULL, 3, '2019-11-13 12:49:02', NULL),
(4, NULL, 4, '2019-11-13 12:54:04', NULL),
(7, 20, 7, '2019-11-20 05:54:42', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `attachments`
--

CREATE TABLE `attachments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attachable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attachable_id` int(10) UNSIGNED NOT NULL,
  `media_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `courses`
--

CREATE TABLE `courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `matter_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL COMMENT '1 yes/ 0=no',
  `short_desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `long_desc` text COLLATE utf8_unicode_ci,
  `indication_admin` text COLLATE utf8_unicode_ci,
  `date_debut` date DEFAULT NULL,
  `date_fin` date DEFAULT NULL,
  `heure_debut` timestamp NULL DEFAULT NULL,
  `heure_fin` timestamp NULL DEFAULT NULL,
  `course_type_id` int(11) DEFAULT '0' COMMENT '0 = normal / 1 = stock',
  `assets_list_id` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `has_chapter` int(11) DEFAULT NULL,
  `chapter` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `courses`
--

INSERT INTO `courses` (`id`, `title`, `content`, `matter_id`, `created_at`, `updated_at`, `user_id`, `is_active`, `short_desc`, `long_desc`, `indication_admin`, `date_debut`, `date_fin`, `heure_debut`, `heure_fin`, `course_type_id`, `assets_list_id`, `image`, `has_chapter`, `chapter`) VALUES
(15, 'TEst', 'fffffffffffffffffffff', 3, '2019-11-07 06:39:06', '2019-11-07 08:41:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
(20, 'Cours MySQL 6', 'Content-', 1, '2019-11-20 03:19:55', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 7, 'course_assets/image/11/1434118735.PNG', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `degree_levels`
--

CREATE TABLE `degree_levels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `studiant_id` int(10) UNSIGNED DEFAULT NULL,
  `test_name` int(11) NOT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_province` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postalcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment_or_questions` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `degree_levels`
--

INSERT INTO `degree_levels` (`id`, `studiant_id`, `test_name`, `address1`, `address2`, `country`, `city`, `state_province`, `postalcode`, `comment_or_questions`, `created_at`, `updated_at`) VALUES
(1, 2, 3, 'Adress1', 'Adress2', 'Pays', 'sfesdf', 'Province', '767', 'asdasd', '2019-10-31 10:50:35', '2019-10-31 10:50:35'),
(2, 3, 4, 'Adress1', 'Adress2', 'Pays test', 'Antananarivo', 'Province', '545444', 'Autres', '2019-11-04 04:33:01', '2019-11-04 04:33:01');

-- --------------------------------------------------------

--
-- Structure de la table `forums`
--

CREATE TABLE `forums` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cripted_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `matters`
--

CREATE TABLE `matters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `matters`
--

INSERT INTO `matters` (`id`, `name`, `description`, `admin_id`, `teacher_id`, `created_at`, `updated_at`) VALUES
(3, 'Test', 'Description', 2, 12, '2019-11-07 09:34:37', '2019-11-09 02:39:05'),
(4, 'Merise', 'Merise une langage de Modélisation', NULL, 12, '2019-11-18 04:00:14', '2019-11-18 04:11:47');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(65, '2014_10_12_000000_create_users_table', 1),
(66, '2014_10_12_100000_create_password_resets_table', 1),
(67, '2019_10_14_114252_create_permission_tables', 1),
(69, '2019_10_15_150551_add_last_login_at_to_users_table', 1),
(70, '2019_10_15_150841_add_last_login_ip_to_users_table', 1),
(71, '2019_10_21_094646_put_new_column_to_users_table', 1),
(72, '2019_10_24_053154_create_forums_table', 1),
(73, '2019_10_24_081051_create_attachments_table', 1),
(74, '2019_10_24_083838_put_new_column_for_attachment', 1),
(75, '2019_10_24_131027_create_people_table', 1),
(76, '2019_10_24_132358_put_foreign_key_into_user_table', 1),
(77, '2019_10_28_075509_put_some_fields_into_people_table', 1),
(78, '2019_10_29_131444_put_some_column_into_the_table_people', 1),
(79, '2019_10_30_090205_create_degree_levels_table', 1),
(80, '2019_10_30_140647_create_table_teacher', 1),
(81, '2019_10_31_113432_put_user_type_into_table_people', 1),
(83, '2019_10_31_132414_put_column_sex_into_table', 1),
(85, '2019_10_15_114209_create_courses_table', 1),
(94, '2019_11_05_153249_create_matters_table', 1),
(96, '2019_11_08_143728_put_column_into_users_table', 2),
(97, '2019_11_09_043318_create_image_table', 3),
(98, '2019_11_14_121702_create_studiants_table', 4),
(99, '2019_11_14_125348_add_table_assignation_cours', 5);

-- --------------------------------------------------------

--
-- Structure de la table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(1, 'App\\User', 8),
(1, 'App\\User', 10),
(2, 'App\\User', 1),
(2, 'App\\User', 2),
(2, 'App\\User', 8),
(2, 'App\\User', 9),
(2, 'App\\User', 10),
(2, 'App\\User', 11),
(3, 'App\\User', 3),
(3, 'App\\User', 5),
(3, 'App\\User', 11),
(3, 'App\\User', 12),
(4, 'App\\User', 4),
(4, 'App\\User', 6),
(4, 'App\\User', 7),
(4, 'App\\User', 13);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `people`
--

CREATE TABLE `people` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` datetime DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postalcode` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationality` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_application` int(11) DEFAULT NULL,
  `user_name_email_application` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` int(11) NOT NULL,
  `sex` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `people`
--

INSERT INTO `people` (`id`, `created_at`, `updated_at`, `name`, `firstname`, `email`, `birthday`, `country`, `city`, `postalcode`, `nationality`, `phone_number`, `email_application`, `user_name_email_application`, `user_type`, `sex`) VALUES
(2, '2019-10-31 10:50:35', '2019-10-31 10:50:35', 'Rakotosoa', 'Feno Zo Tahina', '960gd7@gmail.com', '2019-10-16 00:00:00', 'United State', 'New York', NULL, 'Malagasy', '212454', 2, 'MeWa', 4, 1),
(3, '2019-11-04 04:33:01', '2019-11-04 04:33:01', 'Je fait un test', 'prenom', '1E@gmail.com', '2019-11-21 00:00:00', 'United State', 'Parie', NULL, 'Malagasy', '212454', 2, 'Non', 4, 1),
(4, NULL, NULL, 'Teacher', 'Rakotosoa', '55@hotmail.com', NULL, 'United State', 'Parie df', NULL, NULL, '212454', NULL, NULL, 3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'edit video', 'web', '2019-10-31 09:16:36', '2019-10-31 09:16:36'),
(2, 'delete video', 'web', '2019-10-31 09:16:36', '2019-10-31 09:16:36'),
(3, 'add video', 'web', '2019-10-31 09:16:36', '2019-10-31 09:16:36'),
(4, 'show video', 'web', '2019-10-31 09:16:37', '2019-10-31 09:16:37'),
(5, 'unpublish video', 'web', '2019-10-31 09:16:37', '2019-10-31 09:16:37'),
(6, 'publish video', 'web', '2019-10-31 09:16:37', '2019-10-31 09:16:37'),
(7, 'edit course', 'web', '2019-10-31 09:16:37', '2019-10-31 09:16:37'),
(8, 'delete course', 'web', '2019-10-31 09:16:37', '2019-10-31 09:16:37'),
(9, 'add course', 'web', '2019-10-31 09:16:37', '2019-10-31 09:16:37'),
(10, 'show course', 'web', '2019-10-31 09:16:37', '2019-10-31 09:16:37'),
(11, 'unpublish course', 'web', '2019-10-31 09:16:37', '2019-10-31 09:16:37'),
(12, 'publish course', 'web', '2019-10-31 09:16:37', '2019-10-31 09:16:37'),
(13, 'edit user', 'web', '2019-10-31 09:16:37', '2019-10-31 09:16:37'),
(14, 'delete user', 'web', '2019-10-31 09:16:37', '2019-10-31 09:16:37'),
(15, 'add user', 'web', '2019-10-31 09:16:37', '2019-10-31 09:16:37'),
(16, 'show user', 'web', '2019-10-31 09:16:37', '2019-10-31 09:16:37'),
(17, 'unpublish user', 'web', '2019-10-31 09:16:37', '2019-10-31 09:16:37'),
(18, 'publish user', 'web', '2019-10-31 09:16:37', '2019-10-31 09:16:37');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'web', '2019-10-31 08:56:43', '2019-10-31 08:56:43'),
(2, 'admin', 'web', '2019-10-31 08:56:43', '2019-10-31 08:56:43'),
(3, 'teacher', 'web', '2019-10-31 08:56:44', '2019-10-31 08:56:44'),
(4, 'etudiant', 'web', '2019-10-31 08:56:44', '2019-10-31 08:56:44');

-- --------------------------------------------------------

--
-- Structure de la table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `studiants`
--

CREATE TABLE `studiants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `teachers`
--

CREATE TABLE `teachers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `person_id` int(11) NOT NULL,
  `matter` int(11) DEFAULT NULL,
  `class` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `teachers`
--

INSERT INTO `teachers` (`id`, `person_id`, `matter`, `class`, `created_at`, `updated_at`) VALUES
(1, 4, 1, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `person_id` bigint(20) UNSIGNED DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login_at` timestamp NULL DEFAULT NULL,
  `last_login_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_valid` int(11) DEFAULT NULL,
  `is_connected` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `person_id`, `avatar`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `last_login_at`, `last_login_ip`, `created_at`, `updated_at`, `is_valid`, `is_connected`) VALUES
(5, 4, NULL, 'dev512', '55@hotmail.com', NULL, '$2y$10$Fy4a805gVjKUXkUG8MOSH.0bbVJHyJNBSX6Y94cUrgtx5/DXWip82', NULL, NULL, NULL, '2019-11-04 10:35:04', '2019-11-04 10:35:04', NULL, 0),
(6, 3, NULL, 'dev1514', '1E@gmail.com', NULL, '$2y$10$SGhJfT23Y4pH.6rqJq/ByOIubPuBNslmazZgm0608aMAmParKpVy.', NULL, NULL, NULL, '2019-11-04 10:35:13', '2019-11-04 10:35:13', NULL, 0),
(7, 2, NULL, 'dev56', '960gd7@gmail.com', NULL, '$2y$10$UHTtpG6/ncSvxI2r11sRdePBdeb6MN7hjeuUmuUymDb02uAOvsstm', NULL, NULL, NULL, '2019-11-04 10:35:24', '2019-11-04 10:35:24', NULL, 0),
(10, NULL, NULL, 'superadmin', 'superadmin@gmail.com', NULL, '$2y$10$eghmtExe8Iu7HgUzx3ViwujGGJeOdiVvubJYeId2PY3ti6/dC.TAa', NULL, '2019-11-14 03:35:40', '127.0.0.1', '2019-11-08 14:53:20', '2019-11-14 03:55:22', 1, 0),
(11, NULL, 'uploaded/11/avatar.png', 'admin', 'admin@gmail.com', NULL, '$2y$10$L6bFYBojPoeKVIHrYLRlr.ZmgFiTyLkOEHZoTltapLYXraF9FGCSK', NULL, '2019-11-20 02:48:32', '127.0.0.1', '2019-11-08 14:53:20', '2019-11-20 04:11:39', 1, 1),
(12, NULL, NULL, 'teacher', 'prf114@gmail.com', NULL, '$2y$10$ZUAMZFvwScssk6vEK/SW9OEMqgc8Vgttpr4V2QmsLL9wr0kVppD3C', NULL, '2019-11-18 04:15:08', '127.0.0.1', '2019-11-08 14:53:21', '2019-11-18 04:15:09', 1, 1),
(13, NULL, NULL, 'etudiant', 'participant114@gmail.com', NULL, '$2y$10$q2/UHZBRfwwm3EN5.Pvg3eSQQUhzy4g4y7fjbu4qb4JMqBJszzFr.', NULL, NULL, NULL, '2019-11-08 14:53:21', '2019-11-08 14:53:21', NULL, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `assets_types`
--
ALTER TABLE `assets_types`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `assignment_courses`
--
ALTER TABLE `assignment_courses`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `assoc_assets_course`
--
ALTER TABLE `assoc_assets_course`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `degree_levels`
--
ALTER TABLE `degree_levels`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `forums`
--
ALTER TABLE `forums`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `matters`
--
ALTER TABLE `matters`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Index pour la table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `people_email_unique` (`email`);

--
-- Index pour la table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Index pour la table `studiants`
--
ALTER TABLE `studiants`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_person_id_foreign` (`person_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `assets`
--
ALTER TABLE `assets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `assets_types`
--
ALTER TABLE `assets_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `assignment_courses`
--
ALTER TABLE `assignment_courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `assoc_assets_course`
--
ALTER TABLE `assoc_assets_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `attachments`
--
ALTER TABLE `attachments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `degree_levels`
--
ALTER TABLE `degree_levels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `forums`
--
ALTER TABLE `forums`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `matters`
--
ALTER TABLE `matters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT pour la table `people`
--
ALTER TABLE `people`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `studiants`
--
ALTER TABLE `studiants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_person_id_foreign` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
