-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 15 nov. 2019 à 15:50
-- Version du serveur :  10.1.33-MariaDB
-- Version de PHP :  7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `stripmooc`
--

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `person_id` bigint(20) UNSIGNED DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login_at` timestamp NULL DEFAULT NULL,
  `last_login_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_valid` int(11) DEFAULT NULL,
  `is_connected` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `person_id`, `avatar`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `last_login_at`, `last_login_ip`, `created_at`, `updated_at`, `is_valid`, `is_connected`) VALUES
(5, 4, NULL, 'dev512', '55@hotmail.com', NULL, '$2y$10$Fy4a805gVjKUXkUG8MOSH.0bbVJHyJNBSX6Y94cUrgtx5/DXWip82', NULL, NULL, NULL, '2019-11-04 10:35:04', '2019-11-04 10:35:04', NULL, 0),
(6, 3, NULL, 'dev1514', '1E@gmail.com', NULL, '$2y$10$SGhJfT23Y4pH.6rqJq/ByOIubPuBNslmazZgm0608aMAmParKpVy.', NULL, NULL, NULL, '2019-11-04 10:35:13', '2019-11-04 10:35:13', NULL, 0),
(7, 2, NULL, 'dev56', '960gd7@gmail.com', NULL, '$2y$10$UHTtpG6/ncSvxI2r11sRdePBdeb6MN7hjeuUmuUymDb02uAOvsstm', NULL, NULL, NULL, '2019-11-04 10:35:24', '2019-11-04 10:35:24', NULL, 0),
(10, NULL, NULL, 'superadmin', 'superadmin@gmail.com', NULL, '$2y$10$eghmtExe8Iu7HgUzx3ViwujGGJeOdiVvubJYeId2PY3ti6/dC.TAa', NULL, '2019-11-14 03:35:40', '127.0.0.1', '2019-11-08 14:53:20', '2019-11-14 03:55:22', 1, 0),
(11, NULL, NULL, 'admin', 'admin@gmail.com', NULL, '$2y$10$L6bFYBojPoeKVIHrYLRlr.ZmgFiTyLkOEHZoTltapLYXraF9FGCSK', NULL, '2019-11-15 03:29:57', '127.0.0.1', '2019-11-08 14:53:20', '2019-11-15 03:29:57', 1, 1),
(12, NULL, NULL, 'teacher', 'prf114@gmail.com', NULL, '$2y$10$ZUAMZFvwScssk6vEK/SW9OEMqgc8Vgttpr4V2QmsLL9wr0kVppD3C', NULL, '2019-11-11 04:12:47', '127.0.0.1', '2019-11-08 14:53:21', '2019-11-11 04:12:47', 1, 1),
(13, NULL, NULL, 'etudiant', 'participant114@gmail.com', NULL, '$2y$10$q2/UHZBRfwwm3EN5.Pvg3eSQQUhzy4g4y7fjbu4qb4JMqBJszzFr.', NULL, NULL, NULL, '2019-11-08 14:53:21', '2019-11-08 14:53:21', NULL, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_person_id_foreign` (`person_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_person_id_foreign` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
